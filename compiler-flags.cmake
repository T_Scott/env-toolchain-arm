message("\r\n********** Compiler **********\r\n")

message("C compiler: ${CMAKE_C_COMPILER}")
message("C++ compiler: ${CMAKE_CXX_COMPILER}")
message("ASM compiler: ${CMAKE_ASM_COMPILER}")
message("Linker: ${CMAKE_LINKER}")
message("Objcopy: ${CMAKE_OBJCOPY}")
message("Objdump: ${CMAKE_OBJDUMP}")

message("\r\n********** Compiler Flags **********\r\n")

# Warnings
set(COMPILER_WARNINGS "-Wextra -Wpedantic -Wno-unused-parameter -Wcast-align -Wconversion -Wshadow -Wall -Wdouble-promotion -Wmissing-include-dirs -Wswitch-default -Wuninitialized -Wfloat-equal -Wundef -Wlarger-than=150 -Wstack-usage=150 -Wunsafe-loop-optimizations -Wbad-function-cast -Wlogical-op -Waggregate-return -Wmissing-prototypes -Wmissing-declarations -Wpacked -Wredundant-decls -Wnested-externs -Wvarargs")
set(COMPILER_WARNINGS "-Wextra -Wall -Wno-unused-parameter")

# Common arguments
set(COMMON_DEFINITIONS "${COMPILER_WARNINGS} -mthumb -fno-builtin -ffunction-sections -fdata-sections -fomit-frame-pointer")

# Optimise for size
set(OPTFLAGS "-Os")

# Set libs for C++ if required
if(USE_CXX)
	set(OPTIONAL_LIBS ${OPTIONAL_LIBS} -lstdc++ -lsupc++)
endif()

# Set libs for C
set(OPTIONAL_LIBS ${OPTIONAL_LIBS} -lm -lc -lgcc -lnosys)

# Build flags
set(CMAKE_C_FLAGS "-std=gnu99 ${COMMON_DEFINITIONS} --specs=nano.specs -MMD -MP")
set(CMAKE_CXX_FLAGS "${COMMON_DEFINITIONS} --specs=nano.specs -MMD -MP")
set(CMAKE_ASM_FLAGS "${COMMON_DEFINITIONS} -x assembler-with-cpp")
set(CMAKE_EXE_LINKER_FLAGS "${COMMON_DEFINITIONS} -Xlinker --gc-sections")

# Build specific flags
set(CMAKE_C_FLAGS_RELEASE "-DRELEASE=1 -g -gdwarf-2 -DNDEBUG=1 ${OPTFLAGS}")
set(CMAKE_C_FLAGS_DEBUG "-DDEBUG=1 -O0 -g -gdwarf-2")
set(CMAKE_CXX_FLAGS_RELEASE "-DRELEASE=1 -g -gdwarf-2 -DNDEBUG=1 ${OPTFLAGS}")
set(CMAKE_CXX_FLAGS_DEBUG "-DDEBUG=1 -O0 -g -gdwarf-2")
set(CMAKE_EXE_LINKER_FLAGS_RELEASE "${OPTFLAGS}")

message("C Flags:\r\n${CMAKE_C_FLAGS}\r\n")
message("C++ Flags:\r\n${CMAKE_CXX_FLAGS}\r\n")
message("Assembly Flags:\r\n${CMAKE_ASM_FLAGS}\r\n")
message("Linker Flags:\r\n${CMAKE_EXE_LINKER_FLAGS}")